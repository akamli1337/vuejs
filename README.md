graph TD;
    A[Jenkins Pipeline Start] --> B[Set Environment Variables]
    B --> C[Copy IaC Files to Temporary Directory]
    C --> D[Clone Project from Bitbucket to Jenkins Workspace]
    D --> E[Build & Deploy Docker Environment]
    E --> F[Run docker-compose build & up]
    F --> G[Docker Compose Configuration]
    G --> H[Dockerfile: Apache & PHP Setup]
    H --> I[Post-Build Notifications]

    E -.->|Move IaC files back to main directory| F
    F -.->|Service Configuration in docker-compose.yml| G
    G -.->|Install dependencies & Configure Apache| H
    I -->|Send Success/Failure Notifications| J[Google Chat Notification]